# ovirt-engine-wildfly-overlay

#### 介绍
此软件包包含WildFly 8.2.1的升级部分，修复了影响oVirt引擎的相关bug：

1. hibernate-validator5.2.2.Final
    - https://hibernate.atlassian.net/browse/HV-861

2. apache-commons-collections 3.2.2
    - https://issues.apache.org/jira/browse/COLLECTIONS-580
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

