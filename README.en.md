# ovirt-engine-wildfly-overlay

#### Description
This package contains upgraded parts of WildFly 8.2.1, which fix bugs
affecting oVirt engine:

1. hibernate-validator 5.2.2.Final
     - Fixes https://hibernate.atlassian.net/browse/HV-861
2. apache-commons-collections 3.2.2
     - Fixes https://issues.apache.org/jira/browse/COLLECTIONS-580

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
