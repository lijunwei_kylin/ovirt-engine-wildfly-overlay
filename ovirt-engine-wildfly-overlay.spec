Name:		ovirt-engine-wildfly-overlay
Version:	22.0.0
Release:	1
Summary:	WildFly overlay for ovirt-engine
Group:		Virtualization/Management
License:	ASL-2.0
URL:		http://www.ovirt.org
BuildArch:	noarch
Source0:	README

Requires:	ovirt-engine-wildfly = %{version}-%{release}


%description
WildFly overlay for ovirt-engine


%install
install -d -m 0755 "%{buildroot}%{_docdir}/%{name}"
install -m 0644 "%{SOURCE0}" "%{buildroot}%{_docdir}/%{name}/README"
install -d -m 0755 "%{buildroot}%{_datadir}/%{name}/modules"


%files
%{_datadir}/%{name}/
%{_docdir}/%{name}/


%changelog
* Tue Jul  6 2021 lijunwei <lijunwei@kylinos.cn> - 22.0.0-1
- Update version
* Thu Mar  5 2020 changjie.fu <changjie.fu@cs2c.com.cn> - 17.0.1-1
- Package Initialization
